/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package modal

import (
	"github.com/gdamore/tcell/v2"
	"gitlab.com/tslocum/cview"
)

// Error constructs a modal value that shows an error
// modal dialog and return back a pointer to it
func Error(text string, buttonText string) *cview.Modal {

	modal := cview.NewModal()
	modal.SetBackgroundColor(tcell.ColorTomato).SetTextColor(tcell.ColorWhiteSmoke)

	return modal
}

// Info constructs a modal value that shows information
// modal dialog and return back a pointer to it
func Info(text string, buttonText string) *cview.Modal {

	return dialog(text, []string{buttonText})
}

// Confirm constructs a modal value that shows a confirmation
// modal dialog and return back a pointer to it
func Confirm(text string, buttonsText []string) *cview.Modal {

	return dialog(text, buttonsText)
}

// dialog is a helper function to create modal dialogs
func dialog(text string, buttons []string) *cview.Modal {

	modal := cview.NewModal()
	modal.SetText(text)
	modal.AddButtons(buttons)

	return modal
}

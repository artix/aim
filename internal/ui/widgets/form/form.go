/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package form

import (
	"gitlab.com/tslocum/cview"
)

// Form helps with the construction of AIM forms
type Form struct {
	InnerForm *cview.Form
	Layout    *cview.Flex
}

// New created a new Form value using the given inner form and return
func New(title string, next, prev func()) *Form {

	// create the inner form
	innerForm := cview.NewForm()
	innerForm.SetBorder(true).SetTitle(" " + title + " ")

	// add buttons
	innerForm.AddButton("Previous", prev)
	innerForm.AddButton("Next", next)

	// create flex layout
	layout := cview.NewFlex()

	// add left box
	layout.AddItem(cview.NewBox(), 0, 1, false)

	// add form to the center partition using a new row flex
	layout.AddItem(
		cview.NewFlex().SetDirection(cview.FlexRow).
			AddItem(cview.NewBox(), 0, 1, false).
			AddItem(innerForm, 0, 1, true).
			AddItem(cview.NewBox(), 0, 1, false), 0, 2, true,
	)

	// add right box
	layout.AddItem(cview.NewBox(), 0, 1, false)

	// create form and return a pointer to it
	form := Form{
		InnerForm: innerForm,
		Layout:    layout,
	}

	return &form
}

/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package cover

import (
	"encoding/base64"
	"fmt"
	"strings"

	"gitea.artixlinux.org/artix/aim/internal/ui/sections"
	"github.com/gdamore/tcell/v2"
	"gitlab.com/tslocum/cview"
)

const artixLogo string = `
CiAgICAgICAgICAgICAgICAgICAnICAgICAgICAgICAgICAgICAgIAogICAgICAgICAgICA
gICAgICAnbycgICAgICAgICAgICAgICAgICAKICAgICAgICAgICAgICAgICAnb29vJyAgIC
AgICAgICAgICAgICAgCiAgICAgICAgICAgICAgICAnb294b28nICAgICAgICAgICAgICAgI
AogICAgICAgICAgICAgICAnb294eHhvbycgICAgICAgICAgICAgICAKICAgICAgICAgICAg
ICAnb29va2t4eG9vJyAgICAgICAgICAgICAgCiAgICAgICAgICAgICAnb2lpb3hra3h4b28
nICAgICAgICAgICAgIAogICAgICAgICAgICAnOjs6aWlpaW94eHhvbycgICAgICAgICAgIC
AKICAgICAgICAgICAgICAgYCcuOzo6aW94eG9vJyAgICAgICAgICAgCiAgICAgICAgICAnL
S4gICAgICBgJzo7amlvb28nICAgICAgICAgIAogICAgICAgICAnb29vaW8tLi4gICAgIGAn
aTppbycgICAgICAgICAKICAgICAgICAnb29vb3h4eHhvaW86LC4gICBgJy07JyAgICAgICA
gCiAgICAgICAnb29vb3h4eHh4a2t4b29vSWk6LS4gIGAnICAgICAgIAogICAgICAnb29vb3
h4eHh4a2tra3hvaWlpaWlqaScgICAgICAgICAKICAgICAnb29vb3h4eHh4a3h4b2lpaWk6J
2AgICAgIC5pJyAgICAgCiAgICAnb29vb3h4eHh4b2k6OjonYCAgICAgICAuO2lveG8nICAg
IAogICAnb29vb3hvb2k6OidgICAgICAgICAgLjppaWl4a3h4bycKICAnb29vb2k6J2AgICA
gICAgICAgICAgICAgYCcnO2lveHhvJyAgCiAnaTonYCAgICAgICAgICAgICAgICAgICAgIC
AgICAgJyc6aW8nIAonYCAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYCcK
`

const (
	subtitle  string = "AIM - The Artix Linux Installer Manager"
	copyright string = "Copyright (c) 2020 - 2021, Artix Linux"
	URL       string = "https://www.artixlinux.org"
)

var Cover *sections.Section = sections.New("Welcome", coverHandler)

// coverHandler is this section handler
func coverHandler(app *cview.Application, next, prev func()) (string, cview.Primitive) {

	logo, err := base64.StdEncoding.DecodeString(artixLogo)
	if err != nil {
		panic(fmt.Errorf("while decoding artix logo: %w", err))
	}

	logoLines := strings.Split(string(logo), "\n")
	logoWidth := 0
	logoHeight := len(logoLines) + 1
	for _, line := range logoLines {
		if len(line) > logoWidth {
			logoWidth = len(line)
		}
	}

	logoBox := cview.NewTextView().SetTextColor(tcell.ColorDarkCyan)
	logoBox.SetDoneFunc(func(key tcell.Key) { next() })
	fmt.Fprint(logoBox, string(logo))

	// create a frame for the subtitle and Artix information
	frame := cview.NewFrame(cview.NewBox()).SetBorders(0, 0, 0, 0, 0, 0)
	frame.AddText(subtitle, true, cview.AlignCenter, tcell.ColorWhite)
	frame.AddText("", true, cview.AlignCenter, tcell.ColorWhite)
	frame.AddText(copyright, true, cview.AlignCenter, tcell.ColorWhite)
	frame.AddText(URL, true, cview.AlignCenter, tcell.ColorBlueViolet)

	// create flex layout to hold it all
	flex := cview.NewFlex().SetDirection(cview.FlexRow)
	flex.AddItem(cview.NewBox(), 0, 7, false)
	flex.AddItem(
		cview.NewFlex().AddItem(cview.NewBox(), 0, 1, false).
			AddItem(logoBox, logoWidth, 1, true).
			AddItem(cview.NewBox(), 0, 1, false), logoHeight, 1, true)
	flex.AddItem(frame, 0, 10, false)

	return "Welcome", flex
}

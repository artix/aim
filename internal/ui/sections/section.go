/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package sections

import "gitlab.com/tslocum/cview"

// SectionHandler is a function which returns the section's main primitive
// as well as its ID. It recives a next and a previous functions
// what can be called to advance or retrocede in the installation
type SectionHandler func(app *cview.Application, next, previous func()) (string, cview.Primitive)

// Section is a data structure that holds convenient data for our layout
type Section struct {
	handler SectionHandler
	id      string
}

// New creates a new section value and back a pointer to it
func New(id string, handler SectionHandler) *Section {

	section := Section{
		id:      id,
		handler: handler,
	}

	return &section
}

// ID returns back the section unique identifier
func (sec *Section) ID() string {

	return sec.id
}

// Handler returns back the section handler
func (sec *Section) Handler() SectionHandler {

	return sec.handler
}

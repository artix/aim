/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package localization

import (
	"golang.org/x/text/language"
	"golang.org/x/text/language/display"
)

// supportedLanguage represents a supported language for the installer itself

// supportedLanguages defines the list of supported languages by the installer itself
var supportedLanguages []language.Tag = []language.Tag{
	language.English,
	language.Spanish,
}

// getSupportedLanguagesAsString retrieves the native strings for each supported language
func getSupportedLanguagesAsString() []string {

	languages := make([]string, len(supportedLanguages))
	for i, tag := range supportedLanguages {
		languages[i] = display.Self.Name(tag)
	}

	return languages
}

/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package localization

import (
	"fmt"
	"os"
	"strings"

	"gitea.artixlinux.org/artix/aim/geolocation"
	"gitea.artixlinux.org/artix/aim/internal/ui/multiform"
	"gitea.artixlinux.org/artix/aim/internal/ui/sections"
	"gitea.artixlinux.org/artix/aim/internal/ui/widgets/form"
	"gitea.artixlinux.org/artix/aim/kbd"
	"gitea.artixlinux.org/artix/aim/locales"
	"gitlab.com/tslocum/cview"
	"golang.org/x/text/language/display"
)

// TODO: Create a data structure that holds global state so it can store
//       modals and other widgets and set their value in order to show them

// LocalizationForm is a data structure that stores data about language and keyboard configurations
type LocalizationForm struct {
}

// local page constants
const (
	chooseLanguage       string = "CHOOSE_APPLICATION_LANGUAGE"
	chooseLocales        string = "CHOOSE_INSTALLATION_LOCALES"
	chooseTimezone       string = "CHOOSE_INSTALLATION_TIMEZONE"
	chooseKeyboardLayout string = "CHOOSE_INSTALLATION_KEYBOARD_LAYOUT"
	chooseKeyboardMap    string = "CHOOSE_INSTALLATION_KEYBOARD_MAP"
)

const (
	chooseLanguageStep int = 0x00 + iota
	chooseLocalesStep
	chooseKeyboardLayoutStep
	chooseKeyboardMapStep
	chooseTimezoneStep
)

const (
	i386QwertyIndex int = 0x09
)

// localizationMultiForm embeds a multiform.Multiform to extends its functionality
type localizationMultiForm struct {
	*multiform.MultiForm

	app            *cview.Application
	layouts        kbd.LayoutModels
	selectedLayout *kbd.LayoutModel
}

// chooseLanguageStep creates the language choosing interface using the given geolocation data
func (lmf *localizationMultiForm) chooseLanguageStep(next, prev func()) cview.Primitive {

	// TODO: automatically select the environment configured locale
	// language if it is supported by the installer

	// TODO: make the Cancel button to show a modal dialog
	langForm := form.New("Choose Installation Language", next, prev)

	// get available languages
	languages := getSupportedLanguagesAsString()

	langForm.InnerForm.AddDropDown(
		"Installation Language: ",
		languages, 0,
		func(option string, index int) {
			setInstallationLanguage(supportedLanguages[index])
			lmf.SetChoice(supportedLanguages[index], chooseLanguageStep, false, false)
		},
	)

	return langForm.Layout
}

// chooseLocalesStep created the locales interface for choosing the system locale
func (lmf *localizationMultiForm) chooseLocalesStep(next, prev func()) cview.Primitive {

	geo, geoErr := geolocation.Lookup()
	if geoErr != nil {
		// TODO: show modal dialog here
		panic(fmt.Errorf("Could not infere your location, are you connected to the internet?\n\n%w", geoErr))
	}
	fmt.Println("NeoGeo:", geo)

	locales, err := locales.Load()
	if err != nil {
		// TODO: handle this error gracefully
		panic(err)
	}

	locale := make([]string, 0, len(locales))
	locales.Alphabetically(func(key string) {
		data := locales[key]
		langName := data.Description
		displayName := display.Self.Name(data.Tag)
		if displayName != "" {
			langName = displayName
		}
		locale = append(locale, fmt.Sprintf("%s.UTF-8 (%s - %s)", data.LocaleName, langName, data.Region))
	})

	localesForm := form.New("Choose System Locales", next, prev)

	var index int
	currentLocale, ok := os.LookupEnv("LANG")
	if ok {
		currentLocale = strings.TrimSpace(currentLocale)
		for i := range locale {
			count := strings.Index(locale[i], "(")
			if strings.TrimSpace(locale[i][0:count]) == currentLocale {
				index = i
			}
		}
	}

	localesForm.InnerForm.AddDropDown(
		"System Locales: ",
		locale, index,
		func(option string, index int) {
			// lmf.SetChoice(option, chooseLocalesStep, false, false)
		},
	)

	return localesForm.Layout
}

func (lmf *localizationMultiForm) chooseKeyboardStep(next, prev func()) cview.Primitive {

	kbdForm := form.New("Choose System Keyboard", next, prev)

	// get the available keyboards
	layouts := kbd.LayoutModels{}
	loadErr := layouts.Load()
	if loadErr != nil {
		// TODO: handle this error gracefully
		panic(loadErr)
	}

	topLayouts := make([]string, 0, len(layouts))
	for _, layout := range layouts {
		name := layout.Name
		if layout.Layouts != nil && len(layout.Layouts) > 0 {
			for _, subLayout := range layout.Layouts {
				if subLayout.KeyMaps != nil {
					topLayouts = append(topLayouts, name+"/"+subLayout.Name)
				}
			}
		} else {
			topLayouts = append(topLayouts, name)
		}
	}

	kbdForm.InnerForm.AddDropDown(
		"Keyboard Layout",
		topLayouts, i386QwertyIndex,
		func(option string, index int) {
			// lmf.SetChoice(option, chooseKeyboardLayoutStep, false, false)
			lmf.app.QueueUpdateDraw(lmf.updateVisibleLayouts)
		},
	)

	return kbdForm.Layout
}

func (lmf *localizationMultiForm) chooseKeyboardMapStep(next, prev func()) cview.Primitive {

	kbdForm := form.New("Choose System Keyboard", next, prev)

	if lmf.selectedLayout != nil {

		kbdLayouts := make([]string, 0, len(lmf.selectedLayout.KeyMaps))
		for _, keyMap := range lmf.selectedLayout.KeyMaps {
			kbdLayouts = append(kbdLayouts, keyMap.Name)
		}

		kbdForm.InnerForm.AddDropDown(
			"Keyboard Layout Map",
			kbdLayouts, 0,
			func(option string, index int) {},
		)
	}

	return kbdForm.Layout
}

func (lmf *localizationMultiForm) updateVisibleLayouts() {

	// if layout information is not filled yet
	if lmf.layouts == nil {
		// load all layout information from the disk
		if err := lmf.layouts.Load(); err != nil {
			// if we can not load the keyboard layout panic
			panic(err)
		}
	}

	choice := lmf.GetStepChoice(2)
	splitData := strings.Split(choice.String(), "/")

	for _, layout := range lmf.layouts {
		if layout.Name == splitData[0] {
			lmf.selectedLayout = layout
			break
		}
	}

	if len(splitData) > 1 {
		for _, subLayout := range lmf.selectedLayout.Layouts {
			if subLayout.Name == splitData[1] {
				lmf.selectedLayout = subLayout
				break
			}
		}
	}
}

// Localizationdefines this section
var Localization *sections.Section = sections.New("Region & Language", localizationHandler)

// localizationHandler is the section handler
func localizationHandler(app *cview.Application, next, prev func()) (string, cview.Primitive) {

	mf := localizationMultiForm{
		MultiForm: multiform.New(),
		app:       app,
	}

	mf.AddForm(chooseLanguage, mf.chooseLanguageStep, mf.Switch(chooseLocales), prev).
		AddForm(chooseLocales, mf.chooseLocalesStep, mf.Switch(chooseKeyboardLayout), mf.Switch(chooseLanguage)).
		AddForm(chooseKeyboardLayout, mf.chooseKeyboardStep, mf.Switch(chooseKeyboardMap), mf.Switch(chooseLocales)).
		AddForm(chooseKeyboardMap, mf.chooseKeyboardMapStep, next, mf.Switch(chooseKeyboardLayout))

	return "Region & Language", mf.Primitive()
}

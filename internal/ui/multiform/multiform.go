/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

// This package implements a custom cview widget to ease the construction
// of a multi paged formulary that will support additional features
//
// The motivation to create this widget comes from our need to create many
// different views and panes for many different sections on AIM, it was becoming
// a raising pain to create and mantain all of those

package multiform

import (
	"os"

	"github.com/alecthomas/colour"
	"gitlab.com/tslocum/cview"
)

// Handler is a customary type that defines a function handler
// to use with each different multi form page forms. The first
// func type parameter is a function defining the handler linked
// to the "next" button of the from page while the second one
// is the function handler linked to the previous button
type Handler func(func(), func()) cview.Primitive

// MultiFormChoice is a helper data structure that holds the value of
// a given chosen option in an specific step of a Multi Form
type MultiFormChoice struct {
	// The step on which this choice was made in the multi form
	Step int

	// The Label this option was tagged with
	Label string

	// The value of the choice
	Value interface{}

	// used to know if we have to convert to int or decimal
	numeric, decimal bool
}

// IsNumeric returns true if the given MultiFormChoice is numeric
func (mfc *MultiFormChoice) IsNumeric() bool { return mfc.numeric }

// IsDecimal returns true if the given MultiformChoice is decimal
func (mfc *MultiFormChoice) IsDecimal() bool { return mfc.decimal }

// Numeric returns back this MultiFormChoice value as an integer
func (mfc *MultiFormChoice) Numeric() int { return mfc.Value.(int) }

// Decimal returns back this MultiFormChoice value as a float64
func (mfc *MultiFormChoice) Decimal() float64 { return mfc.Value.(float64) }

// String returns back this MultiFormChoice value as a string
func (mfc *MultiFormChoice) String() string { return mfc.Value.(string) }

// MultiForm allow us to combine multiple forms into a single primitive providing
// of some valuable features that many forms normally require, like having the
type MultiForm struct {
	// The pager to hold the different forms
	pages *cview.Pages

	// Choices register both the name of the choice made and its value
	choices []*MultiFormChoice
}

// New returns a new multiform-based layout container with no initial primitives
func New() *MultiForm {

	mf := MultiForm{
		pages:   cview.NewPages(),
		choices: make([]*MultiFormChoice, 0, 5),
	}

	return &mf
}

// AddForm add a new form page to our MultiForm
func (mf *MultiForm) AddForm(formID string, formHandler Handler, next func(), prev func()) *MultiForm {

	visible := true
	if _, frontPage := mf.pages.GetFrontPage(); frontPage != nil {
		visible = false
	}

	mf.pages.AddPage(formID, formHandler(next, prev), true, visible)
	mf.choices = append(mf.choices, &MultiFormChoice{
		Step: len(mf.choices),
	})

	return mf
}

func (mf *MultiForm) GetStepChoice(step int) *MultiFormChoice {

	// if the given choice step is higher than current multi form choices
	// configured for this multi form, this will panic, we handle it here
	// to tidy up a bit the error message so the user experience is not
	// bad but panic is the expected behavior as if this happens, it means
	// we are facing a data integrity issue
	if len(mf.choices) < step {
		colour.Printf("^1error^R: the given choice step number ^4%d^R is higher than multiform steps (^4%d^R), aborting", step, len(mf.choices))
		os.Exit(1)
	}

	return mf.choices[step]
}

func (mf *MultiForm) SetChoice(value interface{}, step int, numeric, decimal bool) *MultiForm {

	// if the given choice step is higher than current multi form choices
	// configured for this multi form, this will panic, we handle it here
	// to tidy up a bit the error message so the user experience is not
	// bad but panic is the expected behavior as if this happens, it means
	// we are facing a data integrity issue
	if len(mf.choices) < step {
		colour.Printf("^1error^R: the given choice step number ^4%d^R is higher than multiform steps (^4%d^R), aborting", step, len(mf.choices))
		os.Exit(1)
	}

	mf.choices[step].Value = value
	mf.choices[step].numeric = numeric
	mf.choices[step].decimal = decimal
	return mf
}

func (mf *MultiForm) Primitive() cview.Primitive { return mf.pages }

func (mf *MultiForm) Switch(section string) func() {

	return func() {
		mf.pages.SwitchToPage(section)
	}
}

/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package geolocation

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// fedoraGeoIP is a data structure that holds geoIP location data from
// geoip.fedoraproject.org
type fedoraGeoIP struct {
	Ip          string `json:"ip"`
	Countrycode string `json:"country_code"`
	CountryName string `json:"country_name"`
	Timezone    string `json:"time_zone"`
	URL         string
}

// NewFedoraGeoLocater creates a new zero initialized fedora project format
// GeoLocater value and return back a pointer to it
func NewFedoraGeoLocater() *fedoraGeoIP {

	// create a new fedoraGeoIP value
	geo := fedoraGeoIP{URL: "https://geoip.fedoraproject.org/city"}
	return &geo
}

/*
	The following methods implement the GeoLocator interface
*/

func (geo *fedoraGeoIP) IP() string          { return geo.Ip }
func (geo *fedoraGeoIP) CountryCode() string { return geo.Countrycode }
func (geo *fedoraGeoIP) Country() string     { return geo.CountryName }
func (geo *fedoraGeoIP) TimeZone() string    { return geo.Timezone }

// Lookup looks up the current geolocation data
func (geo *fedoraGeoIP) lookup() error {

	resp, err := http.Get(geo.URL)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return fmt.Errorf("remote server answered with a non 200 response: %s", resp.Status)
	}

	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(&geo); err != nil {
		return fmt.Errorf("could not decode fedoraGeoIP JSON data: %w", err)
	}

	return nil
}

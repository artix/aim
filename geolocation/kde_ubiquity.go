/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package geolocation

import (
	"encoding/xml"
	"fmt"
	"net/http"
)

// ubiquityGeoIP is a data structure that holds geoIP location from geoip.kde.org
// Under the hood, what this endpoint is doing is using the
// github.com.oschwald/geoip2-golang package to read the IP data from a
// MaxMind DB GeoIP2 file and output a geoip.ubuntu.com compatible format
type ubiquityGeoIP struct {
	XMLName                                                                xml.Name `xml:"Response"`
	Ip                                                                     string   `xml:"Ip"`
	Status, Countrycode, CountryCode3, CountryName, RegionCode, RegionName string
	City, ZipPostalCode, Latitude, Longitude, AreaCode, Timezone           string
	URL                                                                    string
}

// NewUbiquityGeoLocater creates a new zero initialized ubiquity format
// GeoLocater value and returns back a pointer to it
func NewUbiquityGeoLocater() *ubiquityGeoIP {

	// create a new ubiquityGeoIP value
	geo := ubiquityGeoIP{URL: "https://geoip.kde.org/v1/ubiquity"}
	return &geo
}

/*
	The following methods implement the GeoLocater interface
*/

func (geo *ubiquityGeoIP) IP() string          { return geo.Ip }
func (geo *ubiquityGeoIP) CountryCode() string { return geo.Countrycode }
func (geo *ubiquityGeoIP) Country() string     { return geo.CountryName }
func (geo *ubiquityGeoIP) TimeZone() string    { return geo.Timezone }

// Lookup looks up the current geolocation data
func (geo *ubiquityGeoIP) lookup() error {

	resp, err := http.Get(geo.URL)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return fmt.Errorf("remote server answered with a non 200 response: %s", resp.Status)
	}

	decoder := xml.NewDecoder(resp.Body)
	if err := decoder.Decode(&geo); err != nil {
		return fmt.Errorf("could not decode ubiquityGeoIP XML data: %w", err)
	}

	return nil
}

/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

// Package geolocation is used to find the country and time zone where
// the machine where AIM is running currently is located.
//
// DISCLAIMER:
//   Artix does not use the gathered geolocation information for
//   any other purpose than pre-select the installer language and/or
//   time zone location. We do not store any of that data in any way
//
// NOTE:
//   we try to don't use any intrusive service that could
//   track the users or invade their privacy some how, sadly,
//   nowadays that is a complex task to accomplish but as a
//   general rule of thumb we try to avoid Google and Amazon
//   as much as possible. If the user knows about any abusive
//   treatment of the data from any of the provider we do use
//   please, feel free to open an issue in Artix forums or
//   send us an email about it
//

package geolocation

import "fmt"

// geoLocaters contains a list of the third party geo location
// services we use in order to geo locate the machine, they are tried
// in the same order than they appear in this definition
var geoLocaters = []GeoLocater{
	NewUbiquityGeoLocater(),
	NewFedoraGeoLocater(),
	NewHostIPGeoLocator(),
}

// GeoLocater is the interface all Geo Localization API wrappers must implement
type GeoLocater interface {
	// IP returns back the computer public IP
	IP() string

	// CountryCode returns back the country 2 digits code
	CountryCode() string

	// Country returns back the country name
	Country() string

	// TimeZone returns back the time zone (if any)
	TimeZone() string

	// lookup searchs the current geolocation data
	lookup() error
}

// Lookup tries to geolocalize this machine trying to use different
// third party services in order to do so, if all of them fail,it
// returns a critical error
func Lookup() (GeoLocater, error) {

	// we try to localize the user machine using different ways
	// in order, if all of them fail then we return a critical
	// error back as we understand the machine has no internet
	var lookupErr error
	var locater GeoLocater

	geoLocatersLength := len(geoLocaters) - 1
	for i, geoLocater := range geoLocaters {
		err := geoLocater.lookup()
		if err == nil {
			locater = geoLocater
			break
		}

		if i == geoLocatersLength {
			lookupErr = fmt.Errorf(
				"AIM uses the live Artix Linux repositories in order to install Artix\n" +
					"on the target machine so a working internet connection is required",
			)
		}
	}

	return locater, lookupErr
}

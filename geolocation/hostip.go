/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package geolocation

import (
	"encoding/json"
	"fmt"
	"net/http"
)

// hostipGeoIP is a data structure that holds geoIP location data
// from api.hostip.info/get_json.php
type hostipGeoIP struct {
	Ip          string `json:"ip"`
	Countrycode string `json:"country_code"`
	CountryName string `json:"country_name"`
	Timezone    string
	URL         string
}

// NewHostIPGeoLocator creates a new zero initialized hostip format
// GeoLocater value and return back a pointer to it
func NewHostIPGeoLocator() *hostipGeoIP {

	// create a new hostipGeoIP value
	geo := hostipGeoIP{
		URL:      "https://api.hostip.info/get_json.php",
		Timezone: "unknown",
	}
	return &geo
}

/*
	The following methods implement the GeoLocator interface
*/

func (geo *hostipGeoIP) IP() string          { return geo.Ip }
func (geo *hostipGeoIP) CountryCode() string { return geo.Countrycode }
func (geo *hostipGeoIP) Country() string     { return geo.CountryName }
func (geo *hostipGeoIP) TimeZone() string    { return geo.Timezone }

// Lookup looks up the current geolocation data
func (geo *hostipGeoIP) lookup() error {

	resp, err := http.Get(geo.URL)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return fmt.Errorf("remote server answered with a non 200 response: %s", resp.Status)
	}

	decoder := json.NewDecoder(resp.Body)
	if err := decoder.Decode(&geo); err != nil {
		return fmt.Errorf("could not decode hostipGeoIP JSON data: %w", err)
	}

	return nil
}

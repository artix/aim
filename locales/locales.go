/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package locales

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"golang.org/x/text/language"
)

// localesPath defines a constant string with the path to the Glibc i18n locale files
const localesPath string = "/usr/share/i18n/locales"

// Locale integrates golang.org/x/language/display Dictionaries (if availabe) with Glibc i18n locale data
type Locale struct {
	Name        string
	LocaleName  string
	Description string
	Region      string
	CountryAB2  string
	CountryAB3  string
	Tag         language.Tag
}

// Locales is a customary type to enable sorting and other utilities
type Locales map[string]*Locale

// Alphabetically sorts a Locales map alphabetically
func (l Locales) Alphabetically(operation func(string)) {

	// create a slice of strings to hold keys
	keys := make([]string, 0, len(l))
	for key := range l {
		keys = append(keys, key)
	}

	sort.Strings(keys)

	// execute operation on sorted order
	for _, key := range keys {
		operation(key)
	}
}

// Load loads the locales information and returns it as a list of Locale pointers
func Load() (Locales, error) {

	locales := Locales{}
	files, err := ioutil.ReadDir(localesPath)
	if err != nil {
		return nil, fmt.Errorf("could not open %s for directory traversal: %w", localesPath, err)
	}

	for _, file := range files {

		if file.IsDir() {
			continue
		}

		filename := file.Name()
		if filename == "POSIX" || strings.HasPrefix(filename, "translit") ||
			strings.HasPrefix(filename, "iso") || strings.HasPrefix(filename, "i18n") ||
			strings.HasPrefix(filename, "cns") {
			continue
		}

		name := filename
		if strings.Contains(name, "@") {
			// ignore this locale
			continue
		}

		locale, parseErr := parse(name)
		if parseErr != nil {
			return nil, parseErr
		}

		locales[locale.LocaleName] = locale
	}

	return locales, nil
}

// matching prefixes used to parse glibc i18n locale definition files
var prefixes map[string][]string = map[string][]string{
	"LC_IDENTIFICATION": {"title", "language", "territory"},
	"LC_ADDRESS":        {"country_ab2", "country_ab3"},
}

// parses the glibc i18n locale file and return a pointer to a new Locale value
func parse(filename string) (*Locale, error) {

	file, openErr := os.Open(filepath.Join(localesPath, filename))
	if openErr != nil {
		return nil, fmt.Errorf("could not open '%s' locale file for reading", filename)
	}
	defer file.Close()

	var insection bool
	var current string

	locale := Locale{
		LocaleName: strings.Split(filepath.Base(filename), "@")[0],
	}

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {

		line := scanner.Text()
		if line == "" || len(line) < 3 {
			continue
		}

		if !insection && line[:3] == "LC_" {
			for section := range prefixes {
				if section == strings.TrimSpace(line) {
					insection = true
					current = section
					continue
				}
			}
		}

		if insection {
			if line[:3] == "END" {
				insection = false
				continue
			}

			for _, prefix := range prefixes[current] {

				if strings.HasPrefix(line, prefix) {
					value := strings.TrimSpace(strings.Replace(strings.Replace(line, prefix, "", -1), `"`, "", -1))
					if current == "LC_IDENTIFICATION" {
						switch prefix {
						case "title":
							locale.Description = value
						case "language":
							locale.Name = value
						case "territory":
							locale.Region = value
						}
					} else {
						switch prefix {
						case "country_ab2":
							locale.CountryAB2 = value
						case "country_ab3":
							locale.CountryAB3 = value
						}
					}
				}
			}
		}
	}

	// check if this locale has a language.Tag equivalent and if it
	// does, attach it to it before appending so we can use later
	t, parseErr := language.Parse(locale.LocaleName)
	if parseErr == nil {
		locale.Tag = t
	}

	return &locale, nil
}

/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package locales_test

import (
	"fmt"
	"testing"

	"gitea.artixlinux.org/artix/aim/internal/glyphs"
	"gitea.artixlinux.org/artix/aim/locales"
	"github.com/alecthomas/colour"
	"golang.org/x/text/language"
	"golang.org/x/text/language/display"
)

// TestLocalesLoading tests loading of locales works as expected
func TestLocalesLoading(t *testing.T) {

	tt := []struct {
		name              string
		locale            string
		localeEnglishName string
		localeNativeName  string
		region            string
		country           string
		countryab2        string
		countryab3        string
		tag               language.Tag
	}{
		{
			name:              "American English",
			locale:            "en_US",
			localeEnglishName: "American English",
			localeNativeName:  "American English",
			region:            "United States",
			countryab2:        "US",
			countryab3:        "USA",
			tag:               language.MustParse("en-US"),
		},
		{
			name:              "European Spanish",
			locale:            "es_ES",
			localeEnglishName: "European Spanish",
			localeNativeName:  "español de España",
			region:            "Spain",
			countryab2:        "ES",
			countryab3:        "ESP",
			tag:               language.MustParse("es-ES"),
		},
		{
			name:              "German",
			locale:            "de_DE",
			localeEnglishName: "German",
			localeNativeName:  "Deutsch",
			region:            "Germany",
			countryab2:        "DE",
			countryab3:        "DEU",
			tag:               language.MustParse("de-DE"),
		},
		{
			name:              "Zulu",
			locale:            "zu_ZA",
			localeEnglishName: "Zulu",
			localeNativeName:  "isiZulu",
			region:            "South Africa",
			countryab2:        "ZA",
			countryab3:        "ZAF",
		},
		{
			name:              "Traditional Chinese",
			locale:            "zh_CN",
			localeEnglishName: "Chinese",
			localeNativeName:  "中文",
			region:            "China",
			countryab2:        "CN",
			countryab3:        "CHN",
		},
		{
			name:              "Russian",
			locale:            "ru_RU",
			localeEnglishName: "Russian",
			localeNativeName:  "русский",
			region:            "Russia",
			countryab2:        "RU",
			countryab3:        "RUS",
		},
		{
			name:              "Greek",
			locale:            "el_CY",
			localeEnglishName: "Greek",
			localeNativeName:  "Ελληνικά",
			region:            "Cyprus",
			countryab2:        "CY",
			countryab3:        "CYP",
		},
		{
			name:              "Punjabi",
			locale:            "pa_IN",
			localeEnglishName: "Punjabi",
			localeNativeName:  "ਪੰਜਾਬੀ",
			region:            "India",
			countryab2:        "IN",
			countryab3:        "IND",
		},
		{
			name:              "Japanese",
			locale:            "ja_JP",
			localeEnglishName: "Japanese",
			localeNativeName:  "日本語",
			region:            "Japan",
			countryab2:        "JP",
			countryab3:        "JPN",
		},
	}

	t.Log("Given the need of testing Glibc locales load")
	{

		locales, err := locales.Load()
		if err != nil {
			t.Fatal(colour.Sprintf("^1%s\t^RError: could not load locales: %v", glyphs.Err, err))
		}

		for i, test := range tt {

			tf := func(t *testing.T) {

				t.Log(colour.Sprintf("\t^3Test^R: ^6%d^R\tWhen checking %s (%s)", i, test.locale, test.localeNativeName))
				{
					locale, ok := locales[test.locale]
					if !ok {
						t.Fatal(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\tcould not find locale %s in locales map", glyphs.Err, i, test.locale))
					}

					if got, expected := locale.Region, test.region; got != expected {
						t.Fatal(colour.Sprintf("\t^1%s\t^3Test ^6%d^R\tregion %s was expected but got %s", glyphs.Err, i, expected, got))
					}

					if got, expected := locale.Name, test.localeEnglishName; got != expected {
						t.Fatal(colour.Sprintf("\t^1%s\t^3Test ^6%d^R\tLocale English name %s was expected but got %s", glyphs.Err, i, expected, got))
					}

					if got, expected := locale.CountryAB2, test.countryab2; got != expected {
						fmt.Println(locale)
						t.Fatal(colour.Sprintf("\t^1%s\t^3Test ^6%d^R\tCountryAB2 %s was expected but got %s", glyphs.Err, i, expected, got))
					}

					if got, expected := locale.CountryAB3, test.countryab3; got != expected {
						t.Fatal(colour.Sprintf("\t^1%s\t^3Test ^6%d^R\tCountryAB3 %s was expected but got %s", glyphs.Err, i, expected, got))
					}

					got := display.Self.Name(locale.Tag)
					if got != test.localeNativeName {
						t.Fatal(colour.Sprintf("\t^1%s\t^3Test ^6%d^R\tLocale Native name %s was expected but got %s", glyphs.Err, i, test.localeNativeName, got))
					}

					t.Log(colour.Sprintf("\t^2%s\t^3Test ^6%d^R\tlocale %s loaded as expected", glyphs.Ok, i, test.locale))
				}
			}

			t.Run(test.name, tf)
		}
	}
}

/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package kbd_test

import (
	"fmt"
	"testing"

	"gitea.artixlinux.org/artix/aim/internal/glyphs"
	"gitea.artixlinux.org/artix/aim/kbd"
	"github.com/alecthomas/colour"
)

// TestKbdLoad tests the different keyboard layouts load
func TestKbdLoad(t *testing.T) {

	tt := []struct {
		name      string
		layout    string
		sublayout string
		keymaps   []string
		err       error
	}{
		{
			name:    "Amiga Layout",
			layout:  "amiga",
			keymaps: []string{"amiga-de", "amiga-us"},
		},
		{
			name:    "Atari Layout",
			layout:  "atari",
			keymaps: []string{"atari-de", "atari-se", "atari-uk-falcon", "atari-us"},
		},
		{
			name:      "PC Layout",
			layout:    "i386",
			sublayout: "azerty",
			keymaps:   []string{"azerty", "be-latin1", "fr-latin1", "fr-latin9", "fr-pc", "fr", "wangbe", "wangbe2"},
		},
		{
			name:      "PC Layout",
			layout:    "i386",
			sublayout: "bepo",
			keymaps:   []string{"fr-bepo-latin9", "fr-bepo"},
		},
		{
			name:      "PC Layout",
			layout:    "i386",
			sublayout: "carpalx",
			keymaps:   []string{"carpalx-full", "carpalx"},
		},
		{
			name:      "PC Layout",
			layout:    "i386",
			sublayout: "colemak",
			keymaps:   []string{"colemak"},
		},
		{
			name:      "PC Layout",
			layout:    "i386",
			sublayout: "dvorak",
			keymaps: []string{
				"ANSI-dvorak",
				"dvorak-ca-fr",
				"dvorak-es",
				"dvorak-fr",
				"dvorak-l",
				"dvorak-la",
				"dvorak-no",
				"dvorak-programmer",
				"dvorak-r",
				"dvorak-ru",
				"dvorak-sv-a1",
				"dvorak-sv-a5",
				"dvorak-uk",
				"dvorak-ukp",
				"dvorak",
			},
		},
		{
			name:      "PC Layout",
			layout:    "i386",
			sublayout: "fgGIod",
			keymaps:   []string{"tr_f-latin5", "trf-fgGIod"},
		},
		{
			name:      "PC Layout",
			layout:    "i386",
			sublayout: "olpc",
			keymaps:   []string{"es-olpc", "pt-olpc"},
		},
		{
			name:      "PC Layout",
			layout:    "i386",
			sublayout: "qwerty",
			keymaps: []string{
				"bashkir",
				"bg-cp1251",
				"bg-cp855",
				"bg_bds-cp1251",
				"bg_bds-utf8",
				"bg_pho-cp1251",
				"bg_pho-utf8",
				"br-abnt",
				"br-abnt2",
				"br-latin1-abnt2",
				"br-latin1-us",
				"by-cp1251",
				"by",
				"bywin-cp1251",
				"ca",
				"cf",
				"cz-cp1250",
				"cz-lat2-prog",
				"cz-lat2",
				"cz",
				"defkeymap",
				"defkeymap_V1.0",
				"dk-latin1",
				"dk",
				"emacs",
				"emacs2",
				"es-cp850",
				"es",
				"et-nodeadkeys",
				"et",
				"fa",
				"fi",
				"gr-pc",
				"gr",
				"hu101",
				"il-heb",
				"il-phonetic",
				"il",
				"is-latin1-us",
				"is-latin1",
				"it-ibm",
				"it",
				"it2",
				"jp106",
				"kazakh",
				"ky_alt_sh-UTF-8",
				"kyrgyz",
				"la-latin1",
				"lt.baltic",
				"lt.l4",
				"lt",
				"lv-tilde",
				"lv",
				"mk-cp1251",
				"mk-utf",
				"mk",
				"mk0",
				"nl",
				"nl2",
				"no-latin1",
				"no",
				"pc110",
				"pl",
				"pl1",
				"pl2",
				"pl3",
				"pl4",
				"pt-latin1",
				"pt-latin9",
				"ro",
				"ro_std",
				"ro_win",
				"ru-cp1251",
				"ru-ms",
				"ru-yawerty",
				"ru",
				"ru1",
				"ru2",
				"ru3",
				"ru4",
				"ru_win",
				"ruwin_alt-CP1251",
				"ruwin_alt-KOI8-R",
				"ruwin_alt-UTF-8",
				"ruwin_alt_sh-UTF-8",
				"ruwin_cplk-CP1251",
				"ruwin_cplk-KOI8-R",
				"ruwin_cplk-UTF-8",
				"ruwin_ct_sh-CP1251",
				"ruwin_ct_sh-KOI8-R",
				"ruwin_ct_sh-UTF-8",
				"ruwin_ctrl-CP1251",
				"ruwin_ctrl-KOI8-R",
				"ruwin_ctrl-UTF-8",
				"se-fi-ir209",
				"se-fi-lat6",
				"se-ir209",
				"se-lat6",
				"sk-prog-qwerty",
				"sk-qwerty",
				"sr-cy",
				"sv-latin1",
				"tj_alt-UTF8",
				"tr_q-latin5",
				"tralt",
				"trf",
				"trq",
				"ttwin_alt-UTF-8",
				"ttwin_cplk-UTF-8",
				"ttwin_ct_sh-UTF-8",
				"ttwin_ctrl-UTF-8",
				"ua-cp1251",
				"ua-utf-ws",
				"ua-utf",
				"ua-ws",
				"ua",
				"uk",
				"us-acentos",
				"us",
				"us1",
			},
		},
		{
			name:      "PC Layout",
			layout:    "i386",
			sublayout: "qwertz",
			keymaps: []string{
				"croat",
				"cz-qwertz",
				"cz-us-qwertz",
				"de-latin1-nodeadkeys",
				"de-latin1",
				"de-mobii",
				"de",
				"de_CH-latin1",
				"de_alt_UTF-8",
				"fr_CH-latin1",
				"fr_CH",
				"hu",
				"sg-latin1-lk450",
				"sg-latin1",
				"sg",
				"sk-prog-qwertz",
				"sk-qwertz",
				"slovene",
				"sr-latin",
			},
		},
		{
			name:      "Apple Layout",
			layout:    "mac",
			sublayout: "all",
			keymaps: []string{
				"apple-a1048-sv",
				"apple-a1243-sv-fn-reverse",
				"apple-a1243-sv",
				"apple-internal-0x0253-sv-fn-reverse",
				"apple-internal-0x0253-sv",
				"mac-be",
				"mac-de-latin1-nodeadkeys",
				"mac-de-latin1",
				"mac-de_CH",
				"mac-dk-latin1",
				"mac-dvorak",
				"mac-es",
				"mac-fi-latin1",
				"mac-fr",
				"mac-fr_CH-latin1",
				"mac-it",
				"mac-no-latin1",
				"mac-pl",
				"mac-pt-latin1",
				"mac-se",
				"mac-template",
				"mac-uk",
				"mac-us",
			},
		},
		{
			name:   "Sun Layout",
			layout: "sun",
			keymaps: []string{
				"sun-pl-altgraph",
				"sun-pl",
				"sundvorak",
				"sunkeymap",
				"sunt4-es",
				"sunt4-fi-latin1",
				"sunt4-no-latin1",
				"sunt5-cz-us",
				"sunt5-de-latin1",
				"sunt5-es",
				"sunt5-fi-latin1",
				"sunt5-fr-latin1",
				"sunt5-ru",
				"sunt5-uk",
				"sunt5-us-cz",
				"sunt6-uk",
			},
		},
	}

	t.Log("Given the need to test correct keyboard handling")
	{

		layouts := kbd.LayoutModels{}
		err := layouts.Load()
		if err != nil {
			t.Fatal(colour.Sprintf("\t^1Error^R: could not load keyboard information"))
		}

		for i, test := range tt {
			tf := func(t *testing.T) {

				t.Log(colour.Sprintf("\t^3Test^R: ^6%d^R\tWhen checking %s %s", i, test.layout, test.sublayout))
				{
					layout, err := lookupLayout(test.layout, test.sublayout, layouts)
					if err != nil {
						if test.err == nil {
							t.Fatal(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\tgot unexpected error '%v'", glyphs.Err, i, err))
						}

						if got, expected := err.Error(), test.err.Error(); got != expected {
							t.Fatal(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\twe were expecting error '%v' but got '%v'", glyphs.Err, i, expected, got))
						}

						t.Log(colour.Sprintf("\t^2%s\t^3Test ^6%d^R:\texpected error was raised", glyphs.Ok, i))
					}

					layoutName := test.layout
					if test.sublayout != "" {
						layoutName = test.sublayout
					}

					if got, expected := layout.Name, layoutName; got != expected {
						t.Error(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\tlayout %s was excpected but got %s", glyphs.Err, i, expected, got))
					}

					for _, keymap := range layout.KeyMaps {

						var found bool
						for _, testKeymap := range test.keymaps {
							if testKeymap == keymap.Name {
								found = true
								break
							}
						}

						if !found {
							t.Error(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\tkeymap %s found on kbd package is not found on test table", glyphs.Err, i, keymap.Name))
						} else {
							t.Log(colour.Sprintf("\t^2%s\t^3Test ^6%d^R\tkeymap %s found on kbd package and test data", glyphs.Ok, i, keymap.Name))
						}
					}
				}
			}

			t.Run(test.name, tf)
		}
	}
}

// lookupLayout is a helper function
func lookupLayout(layout, sublayout string, layouts kbd.LayoutModels) (*kbd.LayoutModel, error) {

	var match *kbd.LayoutModel
	for i, l := range layouts {
		if sublayout != "" && (l.Layouts == nil || len(l.Layouts) == 0) {
			continue
		}

		if l.Name == layout {
			match = layouts[i]
			break
		}
	}

	var submatch *kbd.LayoutModel
	if match != nil {
		if sublayout == "" {
			return match, nil
		}

		for i, sl := range match.Layouts {
			if sl.Name == sublayout {
				submatch = match.Layouts[i]
				break
			}
		}
	}

	if submatch != nil {
		return submatch, nil
	}

	return nil, fmt.Errorf("could not find layout %s %s", layout, sublayout)
}

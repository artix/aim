/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package kbd

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"
)

const keymapsPath string = "/usr/share/kbd/keymaps"
const suffixLength int = 7 // .map.gz

// LayoutModels is a customary type to list Keyboard Layout Models and Keymaps
type LayoutModels []*LayoutModel

func (lym *LayoutModels) Load() error {

	files, dirErr := ioutil.ReadDir(keymapsPath)
	if dirErr != nil {
		return fmt.Errorf("could not list keymap layout models in %s", keymapsPath)
	}

	for _, layoutModelName := range files {
		if !layoutModelName.IsDir() || layoutModelName.Name() == "ppc" || layoutModelName.Name() == "include" {
			continue
		}

		layoutModel := LayoutModel{
			Name:    layoutModelName.Name(),
			Layouts: []*LayoutModel{},
		}

		*lym = append(*lym, &layoutModel)
		keyboardLayoutPaths := filepath.Join(keymapsPath, layoutModelName.Name())
		layoutFiles, layoutDirErr := ioutil.ReadDir(keyboardLayoutPaths)
		if layoutDirErr != nil {
			return fmt.Errorf("could not list keymap layouts in %s", keyboardLayoutPaths)
		}

		for _, layoutFile := range layoutFiles {

			if layoutFile.Name() == "include" {
				continue
			}

			if layoutFile.IsDir() {

				innerLayoutModel := LayoutModel{Name: layoutFile.Name()}
				keyboardSubLayoutPaths := filepath.Join(keyboardLayoutPaths, layoutFile.Name())
				subLayoutFiles, subLayoutErr := ioutil.ReadDir(keyboardSubLayoutPaths)
				if subLayoutErr != nil {
					return fmt.Errorf("could not list keymap sublayouts in %s", keyboardSubLayoutPaths)
				}

				for _, subLayoutFile := range subLayoutFiles {
					name := subLayoutFile.Name()
					if !subLayoutFile.IsDir() {
						if name == "include" || !strings.HasSuffix(name, "map.gz") {
							continue
						}

						keymap := KeyMap{Name: name[:len(name)-suffixLength]}
						innerLayoutModel.KeyMaps = append(innerLayoutModel.KeyMaps, &keymap)
					}
				}
				layoutModel.Layouts = append(layoutModel.Layouts, &innerLayoutModel)
				continue
			}

			name := layoutFile.Name()
			if name == "include" || !strings.HasSuffix(name, "map.gz") {
				continue
			}
			keymap := KeyMap{Name: name[:len(name)-suffixLength]}

			layoutModel.KeyMaps = append(layoutModel.KeyMaps, &keymap)
		}
	}

	return nil
}

// LayoutMopdel
type LayoutModel struct {
	Name    string
	Layouts []*LayoutModel
	KeyMaps []*KeyMap
}

// KeyMap
type KeyMap struct {
	Name string
}

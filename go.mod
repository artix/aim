module gitea.artixlinux.org/artix/aim

go 1.14

require (
	github.com/alecthomas/colour v0.1.0
	github.com/alecthomas/kong v0.2.9
	github.com/gdamore/tcell/v2 v2.0.0-dev.0.20200908121250-0c5e1e1720f1
	github.com/magefile/mage v1.9.0
	github.com/mattn/go-isatty v0.0.12 // indirect
	gitlab.com/tslocum/cview v1.4.9
	golang.org/x/text v0.3.3
	gopkg.in/yaml.v2 v2.3.0
)

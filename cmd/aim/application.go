/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package main

import (
	"fmt"
	"strconv"

	"gitea.artixlinux.org/artix/aim/internal/ui/sections"
	"gitea.artixlinux.org/artix/aim/internal/ui/sections/cover"
	"gitea.artixlinux.org/artix/aim/internal/ui/sections/localization"
	"gitea.artixlinux.org/artix/aim/settings"
	"github.com/gdamore/tcell/v2"
	"gitlab.com/tslocum/cview"
)

// The application value
var app = cview.NewApplication()

// current section
var currentSection int = 0

// startApplication is responsible of starting the application wrapper
func startApplication(cfg settings.Options) error {

	// the installer sections
	sections := []*sections.Section{
		cover.Cover,
		localization.Localization,
		// Language,
		// Partitions,
		// Software,
		// Configuration,
		// Bootloader,
		// Finish,
	}

	// set background color
	cview.Styles.PrimitiveBackgroundColor = tcell.ColorDefault

	pages := cview.NewPages()

	// set a bottom bar for displaying information
	info := cview.NewTextView().SetDynamicColors(true).SetRegions(true).SetWrap(false)
	info.SetBackgroundColor(tcell.ColorBlack)

	// create pages navigation functions
	previous := func() {

		section, atoiErr := strconv.Atoi(info.GetHighlights()[0])
		if atoiErr != nil {
			panic(atoiErr)
		}

		if section > 0 {
			section -= 1
			pages.SwitchToPage(sections[section].ID())
			info.Highlight(fmt.Sprintf("%d", section)).ScrollToHighlight()
		}
	}

	next := func() {

		section, atoiErr := strconv.Atoi(info.GetHighlights()[0])
		if atoiErr != nil {
			panic(atoiErr)
		}

		if section < len(sections)-1 {
			section += 1
			pages.SwitchToPage(sections[section].ID())
			info.Highlight(fmt.Sprintf("%d", section)).ScrollToHighlight()
		}
	}

	// create a page for all our sections
	for i, section := range sections {
		id, primitive := section.Handler()(app, next, previous)
		pages.AddPage(id, primitive, true, i == 0)
		fmt.Fprintf(info, `%d ["%d"][darkcyan]%s[white][""] `, i+1, i, id)
	}

	info.Highlight("0")

	// application main layout
	layout := cview.NewFlex().SetDirection(cview.FlexRow)

	// add layout items
	layout.AddItem(pages, 0, 1, true)
	layout.AddItem(info, 1, 1, false)

	// start the application
	return app.SetRoot(layout, true).EnableMouse(true).Run()
}

// var Partitions Section = Section{
// 	handler: func(next, previous func()) (string, cview.Primitive) {
// 		return "Partitions", cview.NewFlex()
// 	},
// 	id: "Partitions",
// }

// var Software Section = Section{
// 	handler: func(next, previous func()) (string, cview.Primitive) {
// 		return "Software", cview.NewFlex()
// 	},
// 	id: "Software",
// }

// var Configuration = Section{
// 	handler: func(next, previous func()) (string, cview.Primitive) {
// 		return "Configuration", cview.NewFlex()
// 	},
// 	id: "Configuration",
// }

// var Bootloader = Section{
// 	handler: func(next, previous func()) (string, cview.Primitive) {
// 		return "Bootloader", cview.NewFlex()
// 	},
// 	id: "Bootloader",
// }

// var Finish = Section{
// 	handler: func(next, previous func()) (string, cview.Primitive) {
// 		return "Finish", cview.NewFlex()
// 	},
// 	id: "Finish",
// }

/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package main

import (
	"fmt"
	"io/ioutil"

	"gitea.artixlinux.org/artix/aim/settings"
	"github.com/alecthomas/colour"
	"gopkg.in/yaml.v2"
)

// StartCmd creates and start a fiber http RESTful service
type StartCmd struct{}

// Run runs the StartCmd command
func (cmd *StartCmd) Run(ctx *Context) error {

	if ctx.Debug {
		colour.Stdout.Printf("^3starting new Artix Install Manager using %s as configuration path^R\n", ctx.ConfigPath)
	}

	// load  yaml configuration from path
	data, ioErr := ioutil.ReadFile(ctx.ConfigPath)
	if ioErr != nil {
		return fmt.Errorf(colour.Sprintf("^1%s^R", ioErr))
	}

	var cfg settings.Options
	unmarshalErr := yaml.Unmarshal(data, &cfg)
	if unmarshalErr != nil {
		return fmt.Errorf("%s: %w", colour.Sprint("^1could not unmarshal yaml^R"), unmarshalErr)
	}

	// start the application
	return startApplication(cfg)
}

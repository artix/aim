/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package main

import (
	"os"
	"os/signal"

	"github.com/alecthomas/kong"
)

type Context struct {
	Debug      bool
	ConfigPath string
}

var cli struct {
	Debug      bool   `help:"enable debug mode"`
	ConfigPath string `help:"configuration path" default:"/etc/aim.conf" type:"path"`

	Start StartCmd `cmd help:"starts the artix linux installation manager using the given configuration path"`
}

// main application entry point
func main() {

	// disable Ctrl+C
	signal.Ignore(os.Interrupt)

	// parse flags
	ctx := kong.Parse(
		&cli,
		kong.Name("aim"),
		kong.Description("Artix Linux Intaller"),
		kong.UsageOnError(),
	)

	// call Run method of selected parsed command
	err := ctx.Run(&Context{Debug: cli.Debug, ConfigPath: cli.ConfigPath})
	ctx.FatalIfErrorf(err)
}

/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package timezone_test

import (
	"strings"
	"testing"

	"gitea.artixlinux.org/artix/aim/internal/glyphs"
	"gitea.artixlinux.org/artix/aim/timezone"
	"github.com/alecthomas/colour"
)

// TestTimeZones tests that timezone handling works as expected
func TestTimeZones(t *testing.T) {

	tt := []struct {
		name   string
		region string
		tzs    []string
	}{
		{
			"Africa TZ",
			"Africa",
			[]string{"Abidjan", "Accra", "Addis_Ababa", "Algiers", "Asmara", "Asmera", "Bamako", "Bangui", "Banjul", "Bissau", "Blantyre", "Brazzaville", "Bujumbura", "Cairo", "Casablanca", "Ceuta", "Conakry", "Dakar", "Dar_es_Salaam", "Djibouti", "Douala", "El_Aaiun", "Freetown", "Gaborone", "Harare", "Johannesburg", "Juba", "Kampala", "Khartoum", "Kigali", "Kinshasa", "Lagos", "Libreville", "Lome", "Luanda", "Lubumbashi", "Lusaka", "Malabo", "Maputo", "Maseru", "Mbabane", "Mogadishu", "Monrovia", "Nairobi", "Ndjamena", "Niamey", "Nouakchott", "Ouagadougou", "Porto-Novo", "Sao_Tome", "Timbuktu", "Tripoli", "Tunis", "Windhoek"},
		},
		{
			"America TZ",
			"America",
			[]string{"Adak", "Anchorage", "Anguilla", "Antigua", "Araguaina", "Argentina/Buenos_Aires", "Argentina/Catamarca", "Argentina/ComodRivadavia", "Argentina/Cordoba", "Argentina/Jujuy", "Argentina/La_Rioja", "Argentina/Mendoza", "Argentina/Rio_Gallegos", "Argentina/Salta", "Argentina/San_Juan", "Argentina/San_Luis", "Argentina/Tucuman", "Argentina/Ushuaia", "Aruba", "Asuncion", "Atikokan", "Atka", "Bahia", "Bahia_Banderas", "Barbados", "Belem", "Belize", "Blanc-Sablon", "Boa_Vista", "Bogota", "Boise", "Buenos_Aires", "Cambridge_Bay", "Campo_Grande", "Cancun", "Caracas", "Catamarca", "Cayenne", "Cayman", "Chicago", "Chihuahua", "Coral_Harbour", "Cordoba", "Costa_Rica", "Creston", "Cuiaba", "Curacao", "Danmarkshavn", "Dawson", "Dawson_Creek", "Denver", "Detroit", "Dominica", "Edmonton", "Eirunepe", "El_Salvador", "Ensenada", "Fortaleza", "Fort_Nelson", "Fort_Wayne", "Glace_Bay", "Godthab", "Goose_Bay", "Grand_Turk", "Grenada", "Guadeloupe", "Guatemala", "Guayaquil", "Guyana", "Halifax", "Havana", "Hermosillo", "Indiana/Indianapolis", "Indiana/Knox", "Indiana/Marengo", "Indiana/Petersburg", "Indiana/Tell_City", "Indiana/Vevay", "Indiana/Vincennes", "Indiana/Winamac", "Indianapolis", "Inuvik", "Iqaluit", "Jamaica", "Jujuy", "Juneau", "Kentucky/Louisville", "Kentucky/Monticello", "Knox_IN", "Kralendijk", "La_Paz", "Lima", "Los_Angeles", "Louisville", "Lower_Princes", "Maceio", "Managua", "Manaus", "Marigot", "Martinique", "Matamoros", "Mazatlan", "Mendoza", "Menominee", "Merida", "Metlakatla", "Mexico_City", "Miquelon", "Moncton", "Monterrey", "Montevideo", "Montreal", "Montserrat", "Nassau", "New_York", "Nipigon", "Nome", "Noronha", "North_Dakota/Beulah", "North_Dakota/Center", "North_Dakota/New_Salem", "Nuuk", "Ojinaga", "Panama", "Pangnirtung", "Paramaribo", "Phoenix", "Port-au-Prince", "Porto_Acre", "Port_of_Spain", "Porto_Velho", "Puerto_Rico", "Punta_Arenas", "Rainy_River", "Rankin_Inlet", "Recife", "Regina", "Resolute", "Rio_Branco", "Rosario", "Santa_Isabel", "Santarem", "Santiago", "Santo_Domingo", "Sao_Paulo", "Scoresbysund", "Shiprock", "Sitka", "St_Barthelemy", "St_Johns", "St_Kitts", "St_Lucia", "St_Thomas", "St_Vincent", "Swift_Current", "Tegucigalpa", "Thule", "Thunder_Bay", "Tijuana", "Toronto", "Tortola", "Vancouver", "Virgin", "Whitehorse", "Winnipeg", "Yakutat", "Yellowknife"},
		},
		{
			"Antarctica TZ",
			"Antarctica",
			[]string{"Casey", "Davis", "DumontDUrville", "Macquarie", "Mawson", "McMurdo", "Palmer", "Rothera", "South_Pole", "Syowa", "Troll", "Vostok"},
		},
		{
			"Arctic TZ",
			"Arctic",
			[]string{"Longyearbyen"},
		},
		{
			"Asia TZ",
			"Asia",
			[]string{"Aden", "Almaty", "Amman", "Anadyr", "Aqtau", "Aqtobe", "Ashgabat", "Ashkhabad", "Atyrau", "Baghdad", "Bahrain", "Baku", "Bangkok", "Barnaul", "Beirut", "Bishkek", "Brunei", "Calcutta", "Chita", "Choibalsan", "Chongqing", "Chungking", "Colombo", "Dacca", "Damascus", "Dhaka", "Dili", "Dubai", "Dushanbe", "Famagusta", "Gaza", "Harbin", "Hebron", "Ho_Chi_Minh", "Hong_Kong", "Hovd", "Irkutsk", "Istanbul", "Jakarta", "Jayapura", "Jerusalem", "Kabul", "Kamchatka", "Karachi", "Kashgar", "Kathmandu", "Katmandu", "Khandyga", "Kolkata", "Krasnoyarsk", "Kuala_Lumpur", "Kuching", "Kuwait", "Macao", "Macau", "Magadan", "Makassar", "Manila", "Muscat", "Nicosia", "Novokuznetsk", "Novosibirsk", "Omsk", "Oral", "Phnom_Penh", "Pontianak", "Pyongyang", "Qatar", "Qostanay", "Qyzylorda", "Rangoon", "Riyadh", "Saigon", "Sakhalin", "Samarkand", "Seoul", "Shanghai", "Singapore", "Srednekolymsk", "Taipei", "Tashkent", "Tbilisi", "Tehran", "Tel_Aviv", "Thimbu", "Thimphu", "Tokyo", "Tomsk", "Ujung_Pandang", "Ulaanbaatar", "Ulan_Bator", "Urumqi", "Ust-Nera", "Vientiane", "Vladivostok", "Yakutsk", "Yangon", "Yekaterinburg", "Yerevan"},
		},
		{
			"Atlantic TZ",
			"Atlantic",
			[]string{"Azores", "Bermuda", "Canary", "Cape_Verde", "Faeroe", "Faroe", "Jan_Mayen", "Madeira", "Reykjavik", "South_Georgia", "Stanley", "St_Helena"},
		},
		{
			"Australia TZ",
			"Australia",
			[]string{"Adelaide", "Brisbane", "Broken_Hill", "Canberra", "Currie", "Darwin", "Eucla", "Hobart					", "Lindeman", "Lord_Howe", "Melbourne", "North", "Perth", "Queensland", "South", "Sydney", "Tasmania", "Victoria", "West", "Yancowinna"},
		},
		{
			"Europe TZ",
			"Europe",
			[]string{"Amsterdam", "Andorra", "Astrakhan", "Athens", "Belfast", "Belgrade", "Berlin", "Bratislava", "Brussels", "Bucharest", "Budapest", "Busingen", "Chisinau", "Copenhagen", "Dublin", "Gibraltar", "Guernsey", "Helsinki", "Isle_of_Man", "Istanbul", "Jersey", "Kaliningrad", "Kiev", "Kirov", "Lisbon", "Ljubljana", "London", "Luxembourg", "Madrid", "Malta", "Mariehamn", "Minsk", "Monaco", "Moscow", "Nicosia", "Oslo", "Paris", "Podgorica", "Prague", "Riga", "Rome", "Samara", "San_Marino", "Sarajevo", "Saratov", "Simferopol", "Skopje", "Sofia", "Stockholm", "Tallinn", "Tirane", "Tiraspol", "Ulyanovsk", "Uzhgorod", "Vaduz", "Vatican", "Vienna", "Vilnius", "Volgograd", "Warsaw", "Zagreb", "Zaporozhye", "Zurich"},
		},
		{
			"Indian TZ",
			"Indian",
			[]string{"Antananarivo", "Chagos", "Christmas", "Cocos", "Comoro", "Kerguelen", "Mahe", "Maldives", "Mauritius", "Mayotte", "Reunion"},
		},
		{
			"Pacific TZ",
			"Pacific",
			[]string{"Apia", "Auckland", "Bougainville", "Chatham", "Chuuk", "Easter", "Efate", "Enderbury", "Fakaofo", "Fiji", "Funafuti", "Galapagos", "Gambier", "Guadalcanal", "Guam", "Honolulu", "Johnston", "Kiritimati", "Kosrae", "Kwajalein", "Majuro", "Marquesas", "Midway", "Nauru", "Niue", "Norfolk", "Noumea", "Pago_Pago", "Palau", "Pitcairn", "Pohnpei", "Ponape", "Port_Moresby", "Rarotonga", "Saipan", "Samoa", "Tahiti", "Tarawa", "Tongatapu", "Truk", "Wake", "Wallis", "Yap"},
		},
	}

	t.Log("Given the need to test correct TimeZone handling")
	{

		zones, err := timezone.LoadDatabase()
		if err != nil {
			t.Fatal(colour.Sprintf("\t^1Error^R: could not list zoneinfo database"))
		}

		t.Log(colour.Sprintf("\t^3Test^R: ^60^R\tWhen loading timezone database"))
		{
			if expected, got := len(tt), len(zones); expected != got {
				t.Error(colour.Sprintf("\t^1%s\t^3Test ^60^R:\tReturned zones length should be %d but it is %d", glyphs.Err, expected, got))
			} else {
				t.Log(colour.Sprintf("\t^2%s\t^3Test ^60^R:\tReturned zones length is correct", glyphs.Ok))
			}
		}

		for i, test := range tt {
			tf := func(t *testing.T) {

				t.Log(colour.Sprintf("\t^3Test^R: ^6%d^R\tWhen iterating over returned zones", i+1))
				{
					region := zones[i]
					if got := region.Name; got != test.region {
						t.Error(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\tExpected region to be %s got %s", glyphs.Err, i+1, test.region, got))
					} else {
						t.Log(colour.Sprintf("\t^2%s\t^3Test ^6%d^R:\tExpected region to be ^2%s^R", glyphs.Ok, i+1, test.region))
					}

					for x := range test.tzs {

						var found bool
						for j := range region.TimeZones {
							if strings.TrimSpace(test.tzs[x]) == strings.TrimSpace(region.TimeZones[j].Name) {
								found = true
								break
							}
						}
						if !found {
							t.Error(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\tExpected %s timezone to be in %s", glyphs.Err, i+1, test.tzs[x], region.Name))
						} else {
							t.Log(colour.Sprintf("\t^2%s\t^3Test ^6%d^R:\tFound timezone %s in %s", glyphs.Ok, i+1, test.tzs[x], region.Name))
						}
					}
				}
			}
			t.Run(test.name, tf)
		}
	}
}

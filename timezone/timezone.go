/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package timezone

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"unicode"
)

const timeZoneDBLocation string = "/usr/share/zoneinfo"

var validTopRegions []string = []string{
	"Africa", "America", "Antarctica", "Arctic", "Atlantic", "Asia", "Australia",
	"Europe",
	"Indian",
	"Pacific",
}

// Region defines a global TZ Region
type Region struct {
	Name      string
	TimeZones []*TimeZone
}

// TimeZone data structure holds information about a timezone
type TimeZone struct {
	Name string
	Path string
	// TODO: add locales
}

// LoadDatabase loads the zoneinfo database from the tzdata package
func LoadDatabase() ([]*Region, error) {

	files, dirErr := ioutil.ReadDir(timeZoneDBLocation)
	if dirErr != nil {
		return nil, fmt.Errorf("could not list %s directory contents", timeZoneDBLocation)
	}

	regions := []*Region{}
	for _, file := range files {

		if !file.IsDir() {
			continue
		}

		regionName := file.Name()
		if !isValidTopRegion(regionName) {
			continue
		}

		timeZonePath := filepath.Join(timeZoneDBLocation, regionName)
		timeZoneFiles, tzDirErr := ioutil.ReadDir(timeZonePath)
		if tzDirErr != nil {
			return nil, fmt.Errorf("could not list %s zone info contents", timeZonePath)
		}

		timezones := []*TimeZone{}
		for _, timeZoneFile := range timeZoneFiles {
			timeZoneInnerPath := filepath.Join(timeZonePath, timeZoneFile.Name())
			if timeZoneFile.IsDir() {
				timeZoneInnerFiles, tzInnerDirErr := ioutil.ReadDir(timeZoneInnerPath)
				if tzInnerDirErr != nil {
					return nil, fmt.Errorf("could not list %s inner zone contents", timeZoneInnerPath)
				}

				for _, timeZoneInnerFile := range timeZoneInnerFiles {
					if isValidTimeZoneName(timeZoneInnerFile.Name()) {
						timezone := TimeZone{
							Name: filepath.Join(timeZoneFile.Name(), timeZoneInnerFile.Name()),
							Path: filepath.Join(timeZoneInnerPath, timeZoneInnerFile.Name()),
						}
						timezones = append(timezones, &timezone)
					}
				}
				continue
			}

			if isValidTimeZoneName(timeZoneFile.Name()) {
				timezone := TimeZone{
					Name: timeZoneFile.Name(),
					Path: timeZoneInnerPath,
				}
				timezones = append(timezones, &timezone)
			}
		}

		region := Region{
			Name:      regionName,
			TimeZones: timezones,
		}
		regions = append(regions, &region)
	}

	return regions, nil
}

// isValidTopRegion returns true if the given region is part of the valid top regions
func isValidTopRegion(region string) bool {

	valid := false
	for _, regionName := range validTopRegions {
		if regionName == region {
			valid = true
			break
		}
	}

	return valid
}

// isValidTimeZoneName returns true if the given timezone appears to have a valid name
func isValidTimeZoneName(timezone string) bool {

	valid := true
	if len(timezone) < 4 {
		for i, c := range timezone {
			if unicode.IsUpper(c) && i > 0 {
				valid = false
			}
		}
	}

	return valid
}

/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package iso369_3

import (
	"encoding/json"
	"fmt"
	"os"
)

// Scope is a customary type used to enumerate language scopes
type Scope int

// UnmarshalJSON implements the JSON Unmarshaler interface
// it provides custom behavior to parse ISO 639-3 Scopes
func (s *Scope) UnmarshalJSON(data []byte) error {

	char := string(data[1])
	scope, ok := scopeMap[char]
	if !ok {
		fmt.Println(scopeMap)
		return fmt.Errorf("could not find %s scope in scopeMap", char)
	}

	*s = scope
	return nil
}

// Type is a customary type to enumerate language types
type Type int

// UnmarshalJSON implements the JSON Unmarshaler interface
// it provides custom behavior to parse ISO 639-3 Types
func (t *Type) UnmarshalJSON(data []byte) error {

	char := string(data[1])
	_type, ok := typeMap[char]
	if !ok {
		return fmt.Errorf("could not find %s type in typeMap", char)
	}

	*t = _type
	return nil
}

// Language Scopes Enumeration
const (
	Individual Scope = iota
	Macrolanguage
	Special
)

// Language Types Enumeration
const (
	Ancient Type = iota
	Constructed
	Extinct
	Historical
	Living
	SpecialType
)

const ISO639_3file string = "/usr/share/iso-codes/json/iso_639-3.json"

// scopeMap is a helper data structure used to convert from iso_639-3 format into ours
var scopeMap map[string]Scope = map[string]Scope{
	"I": Individual, "M": Macrolanguage, "S": Special,
}

// typeMap is a helper data structure used to convert from iso_639-3 format into ours
var typeMap map[string]Type = map[string]Type{
	"A": Ancient, "C": Constructed, "E": Extinct, "H": Historical, "L": Living, "S": SpecialType,
}

// Language data structure represents a language following ISO639-3 standard
// The languages are loaded in runtime from /usr/share/iso-codes/json/iso_639-3.json
type Language struct {
	Name         string `json:"name"`
	InvertedName string `json:"inverted_name,omitempty"`
	Alpha2       string `json:"alpha_2,omitempty"`
	Alpha3       string `json:"alpha_3,omitempty"`
	Scope        Scope  `json:"scope"`
	Type         Type   `json:"type"`
}

// Languages is a convenient data structure to properly parse ISO languages slice
type Languages map[string][]*Language

// Load loads the available languages from iso-codes package
func Load() (Languages, error) {

	file, openErr := os.Open(ISO639_3file)
	if openErr != nil {
		return nil, fmt.Errorf("could not load %q file, is 'iso-codes' package installed?, %w", ISO639_3file, openErr)
	}
	defer file.Close()

	var languages Languages
	return languages, json.NewDecoder(file).Decode(&languages)
}

// Lookup looks for the given language alpha_3 in the give slice of language values
func Lookup(languages Languages, alpha_3 string) (*Language, error) {

	for _, language := range languages["639-3"] {
		if language.Alpha3 == alpha_3 {
			return language, nil
		}
	}

	return nil, fmt.Errorf("could not find language %s in the given languages slice", alpha_3)
}

/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package iso369_3_test

import (
	"encoding/json"
	"fmt"
	"testing"

	"gitea.artixlinux.org/artix/aim/internal/glyphs"
	"gitea.artixlinux.org/artix/aim/language/iso369_3"
	"github.com/alecthomas/colour"
)

// TestLanguageParse tests that Scope and Type are being
// parsed correctly from iso-codes JSON ISO 639-3 data
func TestLanguageParse(t *testing.T) {

	tt := []struct {
		name    string
		alpha_3 string
		scope   iso369_3.Scope
		_type   iso369_3.Type
	}{
		{"English", "eng", iso369_3.Individual, iso369_3.Living},
		{"Scottish Gaelic", "gla", iso369_3.Individual, iso369_3.Living},
		{"Middle Low German", "gml", iso369_3.Individual, iso369_3.Historical},
		{"Kurdish", "kur", iso369_3.Macrolanguage, iso369_3.Living},
		{"Klingon", "tlh", iso369_3.Individual, iso369_3.Constructed},
		{"Esperanto", "epo", iso369_3.Individual, iso369_3.Constructed},
		{"Iberian", "xib", iso369_3.Individual, iso369_3.Ancient},
		{"Sumerian", "sux", iso369_3.Individual, iso369_3.Ancient},
		{"Lower Southern Aranda", "axl", iso369_3.Individual, iso369_3.Extinct},
		{"Undetermined", "und", iso369_3.Special, iso369_3.SpecialType},
	}

	t.Log("Given the need to test correct Scope and Type data parsing from iso_639-3.json")
	{
		languages, err := iso369_3.Load()
		if err != nil {
			t.Fatal(colour.Sprintf("\t^1Error^R: could not load %s file: %v", iso369_3.ISO639_3file, err))
		}

		for i, test := range tt {
			tf := func(t *testing.T) {

				t.Log(colour.Sprintf("\t^3Test^R: ^6%d^R\tWhen checking %q for scope and type", i, test.name))
				{
					lang, lookupErr := iso369_3.Lookup(languages, test.alpha_3)
					if lookupErr != nil {
						t.Fatal(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\tShould be able to lookup language: %v", glyphs.Err, i, lookupErr))
					}

					if scopeGot, typeGot := lang.Scope, lang.Type; scopeGot != test.scope || typeGot != test._type {
						t.Error(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\texpected %d and %d got %d and %d: %v", glyphs.Err, i, test.scope, test._type, scopeGot, typeGot, lang))
					} else {
						t.Log(colour.Sprintf("\t^2%s\t^3Test^R: ^6%d^R:\treceived the expected scope %d and type %d", glyphs.Ok, i, test.scope, test._type))
					}

				}
			}
			t.Run(test.name, tf)
		}
	}
}

// TestMarshalJSONScope tests that Scope is being unmarshal correctly
func TestMarshalJSONScope(t *testing.T) {

	tt := []struct {
		name     string
		scope    string
		expected iso369_3.Scope
		err      error
	}{
		{"Individual", `"I"`, iso369_3.Individual, nil},
		{"Macrolanguage", `"M"`, iso369_3.Macrolanguage, nil},
		{"Special", `"S"`, iso369_3.Special, nil},
		{"Unknown", `"U"`, 0, fmt.Errorf("could not find U scope in scopeMap")},
	}

	for i, test := range tt {
		tf := func(t *testing.T) {

			t.Log(colour.Sprintf("\t^3Test^R: ^6%d^R\ttWhen checking %s Scope unmarshal", i, test.name))
			{
				var scope iso369_3.Scope
				err := json.Unmarshal([]byte(test.scope), &scope)

				if err != nil && test.err == nil {
					t.Fatal(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\tunexpected error %v", glyphs.Err, i, err))
				}

				if test.err != nil && err == nil {
					t.Fatal(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\texpected error %v but got nil", glyphs.Err, i, test.err))
				}

				if test.err != nil && err != nil && test.err.Error() != err.Error() {
					t.Fatal(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\texpected error '%v' but got '%v'", glyphs.Err, i, test.err, err))
				}

				if test.err != nil && err != nil && test.err.Error() == err.Error() {
					t.Log(colour.Sprintf("\t^2%s\t^3Test^R: ^6%d^R\traised expected error '%v'", glyphs.Ok, i, err))
					return
				}

				if scope != test.expected {
					t.Error(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\texected value %v got %v", glyphs.Err, i, test.expected, scope))
				} else {
					t.Log(colour.Sprintf("\t^2%s\t^3Test^R: ^6%d^R\tunmarshaled into expected value %v", glyphs.Ok, i, scope))
				}
			}
		}

		t.Run(test.name, tf)
	}
}

// TestMarshalJSONScope tests that Scope is being unmarshal correctly
func TestMarshalJSONType(t *testing.T) {

	tt := []struct {
		name     string
		_type    string
		expected iso369_3.Type
		err      error
	}{
		{"Ancient", `"A"`, iso369_3.Ancient, nil},
		{"Constructed", `"C"`, iso369_3.Constructed, nil},
		{"Extinct", `"E"`, iso369_3.Extinct, nil},
		{"Historical", `"H"`, iso369_3.Historical, nil},
		{"Living", `"L"`, iso369_3.Living, nil},
		{"SpecialType", `"S"`, iso369_3.SpecialType, nil},
		{"Unknown", `"U"`, 0, fmt.Errorf("could not find U type in typeMap")},
	}

	for i, test := range tt {
		tf := func(t *testing.T) {

			t.Log(colour.Sprintf("\t^3Test^R: ^6%d^R\ttWhen checking %s Type unmarshal", i, test.name))
			{
				var _type iso369_3.Type
				err := json.Unmarshal([]byte(test._type), &_type)

				if err != nil && test.err == nil {
					t.Fatal(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\tunexpected error %v", glyphs.Err, i, err))
				}

				if test.err != nil && err == nil {
					t.Fatal(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\texpected error %v but got nil", glyphs.Err, i, test.err))
				}

				if test.err != nil && err != nil && test.err.Error() != err.Error() {
					t.Fatal(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\texpected error '%v' but got '%v'", glyphs.Err, i, test.err, err))
				}

				if test.err != nil && err != nil && test.err.Error() == err.Error() {
					t.Log(colour.Sprintf("\t^2%s\t^3Test^R: ^6%d^R\traised expected error '%v'", glyphs.Ok, i, err))
					return
				}

				if _type != test.expected {
					t.Error(colour.Sprintf("\t^1%s\t^3Test ^6%d^R:\texected value %v got %v", glyphs.Err, i, test.expected, _type))
				} else {
					t.Log(colour.Sprintf("\t^2%s\t^3Test^R: ^6%d^R\tunmarshaled into expected value %v", glyphs.Ok, i, _type))
				}
			}
		}

		t.Run(test.name, tf)
	}
}

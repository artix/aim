// +build mage

/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package main

import (
	"fmt"
	"os"
	"os/exec"
	"runtime"
	"time"

	"github.com/magefile/mage/mg"
	"github.com/magefile/mage/sh"
)

//
// Required constants and variables
//

const (
	linter      string = "golangci-lint"
	packageName        = "gitea.artixlinux.org/artix/aim/cmd/aim"
	CLDRBase           = "https://unicode.org/Public/cldr/37"
	CLDRVer            = "37.0"
)

var ldflags = "-s -w -X main.commitHash=$COMMIT_HASH -X main.buildtime=$BUILD_TIME -X main.tag=$VERSION_TAG"

var Default = Aim
var binaries = []string{"aim"}

//
// Magefile targets
//

// Clean removes any previous compilation products
func Clean() error {

	if err := sh.Run("go", "clean", "./..."); err != nil {
		return err
	}

	if err := sh.Rm(fmt.Sprintf("./aim.%s", runtime.GOARCH)); err != nil {
		return err
	}

	return nil
}

// Test run tests on project
func Test() error {

	output, err := sh.Output("go", "test", "-v", "./...")
	fmt.Println(output)

	return err
}

// Coverage run tests and outputs a HTML coverage report
func Coverage() error {

	if err := sh.Run("mkdir", "-p", "./cover/"); err != nil {
		return fmt.Errorf("could not create coverage directory: %w", err)
	}

	commitHash := getCommitHash()
	output, err := sh.Output("go", "test", "-coverprofile", fmt.Sprintf("./cover/%s.out", commitHash), "./...")
	if err != nil {
		return fmt.Errorf("could not run coverage test: %w", err)
	}

	fmt.Println(output)
	if err := sh.Run("go", "tool", "cover", "-html", fmt.Sprintf("./cover/%s.out", commitHash), "-o", fmt.Sprintf("./cover/%s.html", commitHash)); err != nil {
		return fmt.Errorf("could not convert coverage report into HTML format: %w", err)
	}

	return nil
}

// Cover run tests and opens a browser to its HTML coverage report
func Cover() error {

	if err := sh.Run("mkdir", "-p", "./cover/"); err != nil {
		return fmt.Errorf("could not create coverage directory: %w", err)
	}

	commitHash := getCommitHash()
	output, err := sh.Output("go", "test", "-coverprofile", fmt.Sprintf("./cover/%s.out", commitHash), "./...")
	if err != nil {
		return fmt.Errorf("could not run coverage test: %w", err)
	}

	fmt.Println(output)
	if err := sh.Run("go", "tool", "cover", "-html", fmt.Sprintf("./cover/%s.out", commitHash)); err != nil {
		return fmt.Errorf("could not convert coverage report into HTML format: %w", err)
	}

	return nil
}

// Linter runs the configured battery of linters over the code
func Linter() error {

	fmt.Printf("running %s...", linter)
	golangciPath, lookupErr := exec.LookPath(linter)
	if lookupErr == nil {
		output, err := sh.Output(golangciPath, "run", "-v")
		fmt.Println(output)
		if err != nil {
			return fmt.Errorf("golangci-lint failed: %w", err)
		}
	}

	return lookupErr
}

// Aim builds the aimed binary
func Aim() error {
	return sh.RunWith(flagEnv(), "go", "build", "-o", fmt.Sprintf("aim.%s", runtime.GOARCH), "-ldflags", ldflags, packageName)
}

// Install installs the aimed binary
func Install() error {

	mg.SerialDeps(Clean, Test, Linter)
	return sh.RunWith(flagEnv(), "go", "install", "-ldflags", ldflags, packageName)
}

//
//  Function helpers
//

// getCommitHash returns the last commit hash using git
func getCommitHash() string {

	hash, _ := sh.Output("git", "rev-parse", "--short", "HEAD")
	return hash
}

// getVersionTag returns the latest version tag defaulting to .release_version
// file is present (declared on CI pipelines) and falling back to git describe
func getVersionTag() string {

	versiontag, _ := sh.Output("git", "describe", "--tags", "--abbrev=0")
	return versiontag
}

// flagEnv creates and return a map with environment variables to inject to the linker
func flagEnv() map[string]string {

	return map[string]string{
		"COMMIT_HASH": getCommitHash(),
		"BUILD_TIME":  time.Now().Format("2006-01-02T15:04:05Z0700"),
		"VERSION_TAG": getVersionTag(),
		"CGO_ENABLED": "1",
	}
}

// fileExists checks that the given file path exists
func fileExists(filename string) bool {

	_, statErr := os.Stat(filename)
	if statErr != nil && os.IsNotExist(statErr) {
		return false
	}

	return true
}

//
// Init
//

func init() {

	os.Setenv("GO111MODULE", "on")
}

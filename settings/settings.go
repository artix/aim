/*
This file is part of AIM the Artix Installer Manager

Copyright (C) 2020 - 2021 Oscar Campos <damnwidget@artixlinux.org>
Copyright (C) 2020 - 2021 Artix Development Team

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <https://www.gnu.org/licenses/>
*/

package settings

// ServerOptions configuration data structure
type ServerOptions struct {
	Address string     `yaml:"listen_address"`
	Port    int        `yaml:"listen_port"`
	TLS     TLSOptions `yaml:"tls_options"`
}

// TLSOptions configuration data structure
type TLSOptions struct {
	CertificateFilePath string `yaml:"cert_file_path"`
	KeyFilePath         string `yaml:"key_file_path"`
	Enabled             bool   `yaml:"enabled"`
}

// Options configuration data structure
type Options struct {
	Server *ServerOptions `yaml:"server_options"`
}
